// Example proving why structs are better than enums

import UIKit

/* Using arrays with strings to check values isn't the best way to do things since the code is vunerables
 * Look below for Enums example
 */

// let days = ["Mon","Tues","Wed","Thurs","Fri","Sat","Sun"]
//
// func weekdayOrWeekend(dayofWeek: String) -> String{
//    
//    switch dayofWeek {
//    
//        case "Mon","Tues","Wed","Thurs","Fri":
//            return "It's a weekday"
//        case "Sat","Sun":
//            return "It's a weekend"
//        default:
//            return "Not a valid day"
//    }
// }

// weekdayOrWeekend(days[5])

/* Using enums
 *
 */

enum Day: Int {
    case Monday, Tuesday, Wednesday, Thursday, Friday, Saturday, Sunday
    
    func daysTillHoliday() -> String {
        
        return "There's \(Day.Saturday.rawValue - self.rawValue) day till your holiday"
    }
    
    init(){
       self = .Tuesday
    }
    
    func dayString() -> String {
        
        switch  self {
        case .Monday:
            return "Monday"
        case .Tuesday:
            return "Tuesday"
        case .Wednesday:
            return "Wednesday"
        case .Thursday:
            return "Thursday"
        case .Friday:
            return "Friday"
        default:
            return "Other day"
        }
    }
}

func weekdayOrWeekend(dayofWeek: Day) -> String{
    
    switch dayofWeek {
        case .Monday, .Tuesday, .Wednesday, .Thursday, .Friday:
            return "It's a weekday"
        case .Saturday, .Sunday:
            return "It's a weekend"
        default:
            "Not day of the week"
    }
}

weekdayOrWeekend(Day.Monday)

var today = Day.Monday
today = .Tuesday


func daysTillWeekend(day: Day) -> String {

    return "There's \(Day.Saturday.rawValue - day.rawValue) day till the weekend"
}

daysTillWeekend(.Friday)

if let firstDayOfWeek = Day(rawValue: 1){

    daysTillWeekend(firstDayOfWeek)
}

/* Using with Associated Values
 * We can use Enums to hold values for verification later
 */


// Will hold or return the string associated with the member value
enum Status {
    case Success(String)
    case Failure(String)
}


// Asssign the member variable and associated string value
let downloadStatus = Status.Failure("Image could not be reached at certain location")

switch downloadStatus {

// Assign a constant to both cases and print the value associated to the member variable
    
    
case .Success(let message):
    print(message)
    
    // This puts the associate value above on line 76 into the constant
case .Failure(let message):
    print(message)
}

/* Methods & Enums
 *
 */


let leaveDate = Day.Tuesday

// Method is declared above in the enum and self is used so it used the variable instance as a reference for its value
leaveDate.daysTillHoliday()


/* Using initalizers
 * When using enums you want to have a default value i.e when a view loads you can use the init() to do this look above at enum
 * Self is used as a reference to the enum it's self
 */

var presentDay = Day()
presentDay.rawValue
presentDay.dayString()


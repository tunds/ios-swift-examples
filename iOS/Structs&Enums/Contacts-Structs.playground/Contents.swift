import UIKit


// This data structure will hold the variables and also the default value for the struct


// It's using a intializiar so that it accepts initial values when you create a new instance of Contact

struct Contact {
    
    
    var firstName: String
    var lastName: String
    var type: String

    init(firstName: String, lastName: String){
     
        self.firstName = firstName
        self.lastName = lastName
        self.type = "Friend"
    }
    
    func getFullName() -> String {
        
        return "\(self.firstName) \(self.lastName)"
    }
    
}

var person = Contact(firstName: "Tunde", lastName: "Adegoroye")
person.getFullName()


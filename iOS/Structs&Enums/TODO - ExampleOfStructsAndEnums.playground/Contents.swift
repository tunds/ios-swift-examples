import UIKit

// TODO list example using structs and enums

enum Status {
    case Doing, Pending, Completed
    
    init() {
        
        self = .Pending
    }
}

struct Task {
    
    var description: String
    var status = Status()
    
    init(description: String){
    
        self.description = description
    }
}



var nextTask = Task(description: "Buy cashew nuts")
nextTask.status = .Completed

// Weather

enum Type {
    case Sunny, Raining, Snowing, Cloudy
    
    init(){
        self = .Cloudy
    }
}

struct Weather {
    var area: String
    var description: String
    var type = Type()
    
    init(area: String, description: String){
        
        self.area = area
        self.description = description
    }
    
}

var todaysWeather = Weather(area: "Manchester", description: "Just a few grey clouds")

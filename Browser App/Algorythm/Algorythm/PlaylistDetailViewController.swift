//
//  PlaylistDetailViewController.swift
//  Algorythm
//
//  Created by Tunde Adegoroye on 30/04/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit

class PlaylistDetailViewController: UIViewController {

    @IBOutlet weak var playListCoverImg: UIImageView!
    @IBOutlet weak var playListTitleLbl: UILabel!
    @IBOutlet weak var playListDescLbl: UILabel!
    
    @IBOutlet weak var playListArtist0: UILabel!
    @IBOutlet weak var playListArtist1: UILabel!
    @IBOutlet weak var playListArtist2: UILabel!
    @IBOutlet weak var playListArtist3: UILabel!
    @IBOutlet weak var playListArtist4: UILabel!
    @IBOutlet weak var playListArtist5: UILabel!
    @IBOutlet weak var playListArtist6: UILabel!
    
    
    var playList: Playlist?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let playList = playList {
        
            playListCoverImg.image = playList.largeIcon
            playListCoverImg.backgroundColor = playList.backgroundColor
            playListTitleLbl.text = playList.title
            playListDescLbl.text = playList.description
            
            playListArtist0.text = playList.artists[0]
            playListArtist1.text = playList.artists[1]
            playListArtist2.text = playList.artists[2]
            playListArtist3.text = playList.artists[3]
            playListArtist4.text = playList.artists[4]
            playListArtist5.text = playList.artists[5]
            playListArtist6.text = playList.artists[6]
            
            
        }

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

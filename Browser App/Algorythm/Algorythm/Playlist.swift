//
//  Playlist.swift
//  Algorythm
//
//  Created by Tunde Adegoroye on 07/05/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import Foundation
import UIKit

struct Playlist {
    
    var title: String?
    var description: String?
    var icon: UIImage?
    var largeIcon: UIImage?
    var artists:[String] = []
    var backgroundColor: UIColor = .clearColor()
    
    init(index: Int){
        let musicLibrary = MusicLibrary()
        let playlistDictionary = musicLibrary.library[index]
        
        title = playlistDictionary["title"] as? String
        
        description = playlistDictionary["description"] as? String
        
        icon = UIImage(named: (playlistDictionary["icon"] as? String)!)

        largeIcon = UIImage(named: playlistDictionary["largeIcon"] as! String)
        
        artists += playlistDictionary["artists"] as! [String]
        
        backgroundColor = rgbaFromDictionary(playlistDictionary["backgroundColor"] as! [String: CGFloat])
    }
    
    func rgbaFromDictionary(colorDictionary: [String: CGFloat]) -> UIColor {
        
        let red = colorDictionary["red"]!
        let blue = colorDictionary["blue"]!
        let green = colorDictionary["green"]!
        let alpha = colorDictionary["alpha"]!
        
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: alpha)
    }
}
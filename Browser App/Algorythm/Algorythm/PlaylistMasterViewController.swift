//
//  ViewController.swift
//  Algorythm
//
//  Created by Tunde Adegoroye on 30/04/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit

class PlaylistMasterViewController: UIViewController {

    var playlistsArray: [UIImageView] = []
    // Bad naming convention DONT USE IN REAL LIFE
    @IBOutlet weak var playlistImageView0: UIImageView!
    @IBOutlet weak var playlistImageView1: UIImageView!
    @IBOutlet weak var playlistImageView2: UIImageView!
    @IBOutlet weak var playlistImageView3: UIImageView!
    @IBOutlet weak var playlistImageView4: UIImageView!
    @IBOutlet weak var playlistImageView5: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Add all the imageviews to the imageview array
       // playlistsArray.append(playlistImageView0)
        
        
        playlistsArray += [ playlistImageView0, playlistImageView1, playlistImageView2,playlistImageView3,playlistImageView4,playlistImageView5]
        
        // Half range operater this will get items from 0 to the count minus 1
        for index in 0..<playlistsArray.count {
            
            // Create an instance of the playlist and get the info from the index
            let playlist = Playlist(index: index)
            
            // From the playlists imageview array at the index set the img and bg color
            playlistsArray[index].image = playlist.icon
            playlistsArray[index].backgroundColor = playlist.backgroundColor
        }

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "showPlaylistDetailSegue" {
            
            // Get the sender which is the tap gesture recogniser associated with the imageview
            let playlistImageView = sender?.view as! UIImageView
            
            // if the imageview exists in the playlists array get the index and pass this onto the detail VC
            if let index = playlistsArray.indexOf(playlistImageView){
                
                let playlistDetailController = segue.destinationViewController as! PlaylistDetailViewController
                playlistDetailController.playList = Playlist(index: index)
            }
        }
    }
    
    
    // The touch gesture recognizer attached to the image when pressed
    @IBAction func showPlayListDetail(sender: AnyObject) {
        performSegueWithIdentifier("showPlaylistDetailSegue", sender: sender)
    }
}


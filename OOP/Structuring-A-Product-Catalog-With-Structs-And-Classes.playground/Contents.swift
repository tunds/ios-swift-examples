import UIKit

// Designated Initializers
// Having this with classes allow you to set the inital value whenever you create an instance of the class
// If they're not constants you can change the values although the instance below is a constant

class Product {
    
    var title: String
    var price: Double = 0.0
    
    init(title: String, price: Double){
        self.title = title
        self.price = price
    }
    
    func changeTitle(newTitle: String) {
        
        title = newTitle
    }
    
    func discountedPrice(percentage: Double) -> Double {
        return price - (price * percentage / 100)
    }
    
}

let drone = Product(title: "Golden Drone", price: 195.99)
drone.title = "Yellow Drone"
drone.price = 80.99

drone.changeTitle("Blue drone")
drone.discountedPrice(10)


// Subclassing

// Rather than using strings for sizes it's best to use an enum
enum Size {
    case Small, Medium, Large
    
    init(){
        self = .Small
    }
}


// Clothing will inherit everything from product since this is the base class its inheriting from
class Clothing: Product {
    
    var size = Size()
    var designer: String
    
    // You can create your own initalizer in your subclass but you must use the super.init to use the (designated) intializer of any values in the base/super class
    init(title: String, price: Double, designer: String){
        self.designer = designer
        super.init(title: title, price: price)
    }
    
    /* Convience inititalizers allow you to set default initializers when you create an instance of a class
     * You can see that the price and designers have default values but you can set the title in the parameter
     */
    convenience init(title: String){
    
        // Using the init method in the subclass and the parameter from the convenience initializer we're able to set values and default values
        self.init(title: title, price: 99, designer: "Clavin Klein")
    }
    
    // Overriding the function from the base class the method must match in order to override a method so just copy and paste it
    // Setting a default value for the parameter percentage
    override func discountedPrice(percentage: Double = 10.0) -> Double {
        
        /* We're using super to get the function from the base class rather than repeating the formula
         * price - (price * percentage / 100)
         * We can access it from one place and this will allow us to easily update it from anywhere as well
         */
        return super.discountedPrice(percentage)
    }
}

// Using designated initializers
var tshirt = Clothing(title: "Red", price: 40, designer: "Supreme")
tshirt.size = .Medium
tshirt.designer
tshirt.discountedPrice()

// Using convinience initializers
var shorts = Clothing(title: "Nike")


// Computed Properties
class Furniture: Product {
    
    var height: Double
    var width: Double
    var length: Double
    
    // This is a getter computed property which will return the length and width of a product when an instance is created
    var surfaceArea: Double {
        return length * width
    }
    
    // Using Setter & Getter
    var tableSurfaceArea: Double {
        
        get {
            return length * width
        }
        set {
            length = newValue
            width = newValue
        }
    }
    
    init(title: String, price: Double, height: Double, width: Double, length: Double){
        
        self.height = height
        self.width = width
        self.length = length
        super.init(title: title, price: price)
    }
}

var table = Furniture(title: "Brown", price: 89.99, height: 10, width: 20, length: 10)
table.surfaceArea


// Optional Properties
class Electronics: Product {
    
    var batteries: Bool?
}


var toy = Electronics(title: "Robot", price: 15)
toy.batteries
toy.batteries = true
if let batteries = toy.batteries {
    if batteries {
        print("It comes with batteries")
    } else {
        print("It doesn't come with batteries")
    }
}


/* Structs/Enums VS Classes
 * Structs & Enums are considered as Value Types meaning they make a copy of themselves and pass it onto another instance
 * Classes are reference Types, they make a reference to that instance in a location of memory rather than copying the value
 * https://developer.apple.com/library/ios/documentation/Swift/Conceptual/Swift_Programming_Language/ClassesAndStructures.html#//apple_ref/doc/uid/TP40014097-CH13-XID_148
 */


class ExampleClass {
    var title: String
    var price: Double
    
    init(title: String, price: Double){
        self.title = title
        self.price = price
    }
}

// You can see here that even though i moved the instance into another variable
// it's still able to change variables in the class since it's a reference to it in memory

var classProduct = ExampleClass(title: "Phone", price: 60)
var classBigProducts = classProduct
classBigProducts.title = "Tablet"
classBigProducts.title
classProduct.title


// You can see here that even though i moved the instance into another variable
// it's not still able to change variables since it's made a copy of the instance in the new variable this is why value types are considered better
struct ExampleStruct {
    var title: String
    var price: Double
}

var structProduct = ExampleStruct(title: "Phone", price: 65)
var bigStructProduct = structProduct
bigStructProduct.title = "Tablet"
bigStructProduct.title
structProduct.title





import UIKit

// Example of OOP using Shapes class

class Shape {
    let sides: Int
    let name: String
    
    init(sides: Int, name: String){
        self.sides = sides
        self.name = name
    }
}

class Square: Shape {
    var sideLength: Double
    var area: Double {
        get {
            return pow(sideLength, 2)
        } set {
            sideLength = sqrt(newValue)
        }
    }
    
    init(sides: Int, name: String, sideLength: Double){
        self.sideLength = sideLength
        super.init(sides: sides, name: name)
    }
    
    convenience init(sideLength: Double){
        self.init(sides: 4, name: "Square", sideLength: sideLength)
    }
}

var square = Square(sideLength: 5)
